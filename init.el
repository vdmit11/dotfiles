;;; flycheck --- shut the fuck up

;; reduce the frequency of garbage collection by making it happen on
;; each 50MB of allocated data (the default is on every 0.76MB)
;; This is needed to just make Emacs start up faster.
(setq gc-cons-threshold 50000000)

;; Always load newest byte code
(setq load-prefer-newer t)


;;; ==================
;;; Package management
;;; ==================

;; Initialize the package system
(require 'package)
(setq package-archives '(("gnu" . "https://elpa.gnu.org/packages/")
                         ("melpa" . "https://melpa.org/packages/")))

(package-initialize)

;; Fetch the list of available packages
(unless package-archive-contents
  (package-refresh-contents))

;; Install the "use-package" package.
;; It is used to install and cofigure all other packages.
(unless (package-installed-p 'use-package)
  (package-install 'use-package))

(use-package use-package
  :custom
  (use-package-always-ensure t "Install packages implicitly whenever (use-package) is triggered"))

;; A better replacement for `list-packages`
(use-package paradox
  :commands
  (paradox-list-packages
   paradox-upgrade-packages)
  :custom
  (paradox-execute-asynchronously t)
  (paradox-github-token t)
  (paradox-automatically-star t))


;;; =====================
;;; basic Emacs UI tweaks
;;; ======================

;; hide tool/menu bars, they are useless, because I don't use the mouse often
(tool-bar-mode -1)
(menu-bar-mode -1)
(scroll-bar-mode -1)

;; scroll buffer line-by-line, because for me it is hard to track wider steps
(setq scroll-step 1)

;; disable annoying splash-screens and hello-messages
(setq inhibit-splash-screen t
      inhibit-startup-screen t
      inhibit-startup-echo-area-message t
      initial-scratch-message nil)

;; disable GUI dialogs, because I prefer text UI
(setq use-file-dialog nil
      use-dialog-box nil)

;; use 'y/n', typing 'yes'/'no' is too damn many keystrokes
(fset 'yes-or-no-p 'y-or-n-p)

;; fringes: make them thinner and more informative
(setq indicate-empty-lines t
      indicate-buffer-boundaries 'left
      visual-line-fringe-indicators '((left-curly-arrow)))
(fringe-mode '(5 . 0))

;; warn when opening files bigger than 100MB
(setq large-file-warning-threshold 100000000)

;; disable the annoying bell ring
(setq ring-bell-function 'ignore)

;; disable junk temporary files, they confuse watching test runners
(setq create-lockfiles nil
      auto-save-default nil
      make-backup-files nil
      backup-directory-alist nil)

;; disable Emacs clipboard manager, because I already use an external manager (Clipit), so two managers conflict
(setq x-select-enable-clipboard-manager nil)

;; open URLs in Chrome  (mostly needed for Elfeed - RSS client)
(setq browse-url-browser-function 'browse-url-chrome)


;; ====================
;; General Emacs tweaks
;; ====================

;; 4 spaces per tab
(setq tab-width 4)

;; disable word-wrapping
(global-visual-line-mode -1)

(use-package exec-path-from-shell
  :config
  (exec-path-from-shell-initialize)
  (exec-path-from-shell-copy-envs
   (list "HOME"
         "GEM_HOME"
         "GEM_PATH"
         "JAVA_HOME"
         "PROJECT_HOME"
         "WORKON_HOME"
         "VIRTUALENVWRAPPER_PYTHON"
         "VIRTUALENVWRAPPER_SCRIPT")))

(use-package server
  :if (display-graphic-p)
  :init
  (defun server-start-unless-running ()
    (unless (server-running-p)
      (server-start)))
  (add-hook 'after-init-hook 'server-start-unless-running))


;; ================
;; fonts and colors
;; ================

;; Font size
(set-face-attribute 'default nil :height 100)

;; Color theme
(use-package zenburn-theme
  :custom
  (zenburn-override-colors-alist
   '(("zenburn-bg-2"  . "grey9")
     ("zenburn-bg-1"  . "grey12")
     ("zenburn-bg-05" . "grey14")
     ("zenburn-bg"    . "grey18")
     ("zenburn-bg+05" . "grey21")
     ("zenburn-bg+1"  . "grey23")
     ("zenburn-bg+2"  . "grey26")
     ("zenburn-bg+3"  . "grey29")))
  :config
  (load-theme 'zenburn t))


;;; =========
;;; Mode Line
;;; =========

;; show current line/column
(line-number-mode t)
(column-number-mode t)
(size-indication-mode t)

(use-package smart-mode-line
  :custom
  (sml/theme 'respectful)
  (sml/no-confirm-load-theme t)
  :config
  (sml/setup))


;;; =========
;;; Highlight
;;; =========

;; Highlight matching parenthesies (built-in)
(use-package paren
  :ensure nil
  :hook (prog-mode . show-paren-mode)
  :custom
  (show-paren-delay 0))

;; Colorize pairs of parenthesies (change color with increasing nesting level).
(use-package rainbow-delimiters
  :commands rainbow-delimiters-mode
  :hook (prog-mode . rainbow-delimiters-mode))

;; Print matching parenthesis in minibuffer (when it is out of the visible area).
(use-package mic-paren
  :config
  (paren-activate))

;; Highlight nearest parentheses surrounding the current cursor position.
(use-package highlight-parentheses
  :commands highlight-parentheses-mode
  :hook (prog-mode . highlight-parentheses-mode))

;; Highlight current line
(use-package hl-line
  :commands hl-line-mode
  :hook (prog-mode . hl-line-mode))

;; Highlight current word
(use-package highlight-thing
  :commands highlight-thing-mode
  :hook (prog-mode . highlight-thing-mode))

;; Highlight all digits in the source code
(use-package highlight-numbers
  :commands highlight-numbers-mode
  :hook (prog-mode . highlight-numbers-mode))


;;; ====
;;; Helm
;;; ====

(use-package etags
  :ensure nil
  :custom
  (tags-add-tables nil "don't ask annoying questions about adding new TAGS tables")
  (tags-revert-without-query t "also don't ask questions about reverting"))

(use-package helm
  :diminish helm-mode
  :bind (("M-x" . helm-M-x)
         ("M-y" . helm-show-kill-ring)
         ("C-x b" . helm-mini)
         ("C-x C-b" . helm-buffers-list)
         ("C-x C-f" . helm-find-files)
         ("C-c t" . helm-etags-select)
         :map helm-map
         ;; swap C-z and TAB default bindings
         ("C-z" . helm-select-action)
         ("<tab>" . helm-execute-persistent-action)
         ("C-i" . helm-execute-persistent-action)) ; TAB in terminal
  :custom
  (helm-split-window-inside-p t)
  :hook
  ((after-init . helm-mode)
   (after-init . helm-adaptive-mode)))


;;; ========================
;;; Help and Discoverability
;;; ========================

(use-package helm-descbinds
  :bind ("C-c h d" . helm-descbinds))

(use-package which-key
  :hook (after-init . which-key-mode))


;;; ===================
;;; search/jump/replace
;;; ===================


(use-package helm-swoop
  :bind (("C-s" . helm-swoop)
         ("C-c a s" . helm-swoop)
         ("C-c a S" . helm-multi-swoop-projectile)))

(use-package helm-ag
  :bind ("C-c a a" . helm-ag)
  :custom
  (helm-ag-use-agignore t)
  (helm-ag-use-grep-ignore-list t))

;; Jump to definition that works with many languages
(use-package dumb-jump
  :commands dumb-jump-mode
  :bind (("C-c a d" . dumb-jump-go))
  :hook (prog-mode . dumb-jump-mode)
  :custom
  (dumb-jump-selector 'helm))

(use-package avy
  :commands avy-setup-default
  :bind (("C-c a j" . avy-goto-word-0)
         ("C-c a l" . avy-goto-line)
         ("C-c a c" . avy-goto-char)
         ("C-c a k" . avy-kill-whole-line)
         ("C-c a K" . avy-kill-region)
         ("C-c a w" . avy-kill-ring-save-whole-line)
         ("C-c a W" . avy-kill-ring-save-region)
         ("C-c a ," . avy-pop-mark))
  :hook (after-init . avy-setup-default)
  :custom
  (avy-background t))

;; Quick jump to the last edit place
(use-package goto-last-change
  :bind (("C-c a q" . goto-last-change)
         ("C-q" . goto-last-change)))


;;; =======================
;;; Editing tools/shortcuts
;;; =======================

;; select block/function/class, expanding from the current edit position
(use-package expand-region
  :bind (("C-c e e" . er/expand-region)
         :map prog-mode-map
         ("C-=" . er/expand-region)))

;; multiedit
(use-package iedit
  :bind (("C-c e i" . iedit-mode)
         ("C-;" . iedit-mode)))

;; toggle comments
(use-package smart-comment
  :bind (("C-c e c" . smart-comment)
         :map prog-mode-map
         ("M-;" . smart-comment)))

;; toggle quotes
;; toggle single/double quotes
(use-package toggle-quotes
  :bind (("C-c e q" . toggle-quotes)
         :map prog-mode-map
         ("C-'" . toggle-quotes)))

;; Move words, lines and regions with
(use-package drag-stuff
  :commands drag-stuff-mode
  :hook
  (prog-mode . drag-stuff-mode)
  :config
  (drag-stuff-define-keys))


;; Smart parentheses - auto-closing, balancing, navigation, etc
(use-package smartparens
  :commands smartparens-mode
  :hook (prog-mode . smartparens-mode))


(use-package align
  :ensure nil
  :bind (("C-c e a" . align)
         ("C-c e A" . align-regexp)))

(use-package multiple-cursors
  :bind (("C-c e m" . mc/mark-all-dwim)))


(use-package zop-to-char
  :bind (("M-z" . zop-to-char)
         ("M-Z" . zop-up-to-char)))


;;; ================================
;;; clipboard, copy/paste, kill/yank
;;; ================================

(setq kill-whole-line t)

(use-package copy-as-format
  :bind (("C-c w h" . copy-as-format-hipchat)
         ("C-c w g" . copy-as-format-github)
         ("C-c w m" . copy-as-format-markdown)))


(defun copy-current-line-position-to-clipboard ()
  "Copy current line in file to clipboard as '</path/to/file>:<line-number>'."
  (interactive)
  (let ((path-with-line-number
         (concat (buffer-file-name) ":" (number-to-string (line-number-at-pos)))))
    (gui-select-text path-with-line-number)
    (kill-new path-with-line-number)
    (message (concat path-with-line-number " copied to clipboard"))))

(define-key global-map (kbd "C-c w p") 'copy-current-line-position-to-clipboard)


;;; =========
;;; undo/redo
;;; =========

;; fix hang when I hit Ctrl+z (common undo key combination)
(global-unset-key (kbd "C-z"))

;; undo/redo using a tree (with nice GUI visualization)
(use-package undo-tree
  :commands global-undo-tree-mode
  :hook (after-init . global-undo-tree-mode)
  :diminish undo-tree-mode
  :custom
  (undo-tree-visualizer-diff t))


;;; ============================
;;; Whitespace and indentication
;;; ============================

(setq-default indent-tabs-mode nil)   ;; don't use tabs to indent
(setq-default tab-width 4)            ;; but maintain correct appearance
(setq require-final-newline t)        ;; Newline at end of file


(use-package whitespace
  :commands whitespace-mode
  :hook (prog-mode . whitespace-mode)

  :config
  (setq whitespace-display-mappings '((tab-mark 9 [187 9]))
        whitespace-style '(face
                           trailing
                           tabs
                           empty
                           space-before-tab
                           tab-mark))

  (face-spec-reset-face 'whitespace-trailing)
  (set-face-background 'whitespace-trailing "grey20")

  (face-spec-reset-face 'whitespace-empty)
  (set-face-background 'whitespace-empty "grey20")

  (face-spec-reset-face 'whitespace-tab)
  (set-face-background 'whitespace-tab "grey20")
  (set-face-foreground 'whitespace-tab "grey30"))

;; Delete more-than-one spaces with the single keystroke
(use-package hungry-delete
  :commands hungry-delete-mode
  :hook (prog-mode . hungry-delete-mode))

;; Clean trailing whitespace from edited lines
(use-package ws-butler
  :commands ws-butler-mode
  :hook (prog-mode . ws-butler-mode))

;; Clean indent whitespace from empty (auto-indented) lines
(use-package clean-aindent-mode
  :commands clean-aindent-mode
  :hook (prog-mode . clean-aindent-mode)
  :bind (:map prog-mode-map
              ("RET" . newline-and-indent)
              ("C-j" . 'newline)))


;;; ========
;;; org-mode
;;; ========

(use-package org
  :ensure nil
  :commands org-mode
  :bind (("C-c o l" . org-store-link)
         ("C-c o a" . org-agenda)
         ("C-c o c" . org-capture)
         ("C-c o b" . org-iswitchb))
  :custom
  (org-agenda-files '("~/.org"))
  ;;(initial-buffer-choice org-default-notes-file)
  (org-default-notes-file "~/.org/notes.org"))


;;; =======================
;;; file/project management
;;; =======================

(use-package projectile
  :commands projectile-global-mode
  :hook (after-init . projectile-mode)
  :custom
  (projectile-completion-system 'helm)
  (projectile-use-git-grep t))

(use-package helm-projectile
  :commands helm-projectile-on
  :hook (after-init . helm-projectile-on))

;; Support `.envrc` files in project directories
(use-package projectile-direnv
  :commands projectile-direnv-export-variables
  :hook (projectile-mode . projectile-direnv-export-variables))


;;; ===============
;;; Version Control
;;; ===============

;; revert buffers automatically when underlying files are changed externally
(global-auto-revert-mode t)

;; diff/patch
(use-package ediff
  :commands ediff
  :custom
  (ediff-window-setup-function 'ediff-setup-windows-plain "Avoid the crazy multi-frames setup")
  (ediff-split-window-function 'split-window-horizontally "Place buffers side-by-side..."))

;; directory diff
(use-package ztree
  :commands ztree-diff
  :custom
  (ztree-draw-unicode-lines t))

;; fringe markers for uncommitted changes
(use-package diff-hl
  :commands
  (diff-hl-mode
   diff-hl-dired-mode)
  :hook
  ((prog-mode . diff-hl-mode)
   (dired-mode . diff-hl-dired-mode)))

;; git
(use-package git-timemachine
  :bind (("C-c g t" . git-timemachine)))

(use-package git-link
  :bind (("C-c g l" . git-link)
         ("C-c g L" . git-link-commit)))

(use-package helm-ls-git
  :bind (("C-c g f" . helm-ls-git-ls)))

(use-package magit
  :bind (("C-c g b" . magit-blame))
  :custom
  (magit-save-repository-buffers 'dontask))

(use-package yagist
  :commands
  (yagist-list
   yagist-region
   yagist-buffer))


;;; ==================================
;;; General Programming Language tools
;;; ==================================

;; Debugger
(use-package realgud
  :bind (:map prog-mode-map
              ("C-c d p" . realgud:pdb)
              ("C-c d P" . realgud:pdb-remote)))

;; Static code analysis
(use-package flycheck
  :commands flycheck-mode
  :hook (prog-mode . flycheck-mode)
  :custom
  (flycheck-check-syntax-automatically '(mode-enabled save) "check only on save, not on the fly")
  (flycheck-temp-prefix ".flycheck_" "name files with dot, so that they hidden in magit/dired/etc"))

;; Snippets
(use-package yasnippet
  :commands yas-global-mode
  :hook (after-init . yas-global-mode)
  :config
  (use-package yasnippet-snippets))

;; Auto-completion
(use-package company
  :defines company-backends
  :commands company-mode
  :hook (prog-mode . company-mode)
  :bind (:map company-mode-map
              ("<backtab>" . 'company-yasnippet))
  :custom
  (company-backends '(company-capf
                      company-etags
                      company-dabbrev-code))
  (company-frontends '(company-pseudo-tooltip-unless-just-one-frontend
                       company-preview-if-just-one-frontend
                       company-quickhelp-frontend))
  (company-auto-complete '(company-explicit-action-p))
  (company-auto-complete-chars nil)
  (company-minimum-prefix-length 2)
  (company-show-numbers t))

(use-package company-quickhelp
  :commands company-quickhelp-mode
  :hook (company-mode . company-quickhelp-mode)
  :custom
  (company-quickhelp-max-lines 80)
  (company-quickhelp-use-propertized-text t))

(use-package company-statistics
  :commands company-statistics-mode
  :hook (company-mode . company-statistics-mode))

(use-package company-try-hard
  :commands company-try-hard
  :init
  (global-set-key (kbd "C-M-i") 'company-try-hard)
  (global-set-key (kbd "M-<tab>") 'company-try-hard))

;;; Clojure
;;; -------

(use-package clojure-mode
  :mode "\\.clj\\'"
  :custom
  (clojure-indent-style :align-arguments))

(use-package cider
  :commands
  (cider-mode
   cider-auto-test-mode)
  :hook
  ((cider-mode . cider-auto-test-mode)
   (cider-mode . eldoc-mode))
  :custom
  (cider-debug-display-locals t)
  (cider-eldoc-display-context-dependent-info t)
  (cider-repl-use-pretty-printing t)
  (cider-prompt-for-symbol t)
  (cider-save-file-on-load t)
  (cider-jdk-src-paths '("~/.local/jdk" "~/.local/src/clojure/src/jvm")))

(use-package clojure-mode-extra-font-locking)
(use-package clojure-snippets)
(use-package kibit-helper)

(use-package flycheck-clojure
  :commands flycheck-clojure-setup
  :hook (clojure-mode . flycheck-clojure-setup))

(use-package clj-refactor
  :commands clj-refactor-mode
  :hook (clojure-mode . clj-refactor-mode))

(use-package helm-cider
  :commands helm-cider-mode
  :hook (clojure-mode . helm-cider-mode))

(use-package cljr-helm
  :bind (:map clojure-mode-map
              ("C-c r" . cljr-helm)))


;;; Python
;;; ------

(use-package python-mode
  :init
  (defun fix-python-annoying-capf ()
    (setq completion-at-point-functions '(t)))
  (add-hook 'python-mode-hook 'fix-python-annoying-capf)

  :bind (:map python-mode-map
              ;; rebind completion from TAB to Alt+TAB
              ([tab] . py-indent-line)
              ([M-tab] . company-try-hard)
              ([C-M-i] . company-try-hard))
  :custom
  (python-environment-directory "~/.local/venvs")
  (python-shell-interpreter "ipython")
  (python-shell-interpreter-args "-i --simple-prompt")
  (python-check-command "pylint")
  (python-shell-completion-native-enable nil))

(use-package pyvenv
  :commands pyvenv-tracking-mode
  :hook (after-init . pyvenv-tracking-mode))

(use-package auto-virtualenvwrapper
  :commands auto-virtualenvwrapper-activate
  :hook (python-mode . auto-virtualenvwrapper-activate))

(use-package elpy
  :commands elpy-enable
  :hook (after-init . elpy-enable)
  :custom
  (elpy-modules '(elpy-module-eldoc
                  elpy-module-pyvenv
                  elpy-module-highlight-indentation
                  elpy-module-yasnippet
                  elpy-module-sane-defaults))
  (elpy-test-runner 'elpy-test-nose-runner))

(use-package flycheck
  :config
  (defun set-pylint-as-flycheck-checker ()
    (setq flycheck-checker 'python-pylint))
  (add-hook 'python-mode-hook 'set-pylint-as-flycheck-checker)
  (flycheck-add-mode 'python-pylint 'python-mode))

(use-package python-docstring
  :commands python-docstring-mode
  :hook (python-mode . python-docstring-mode))

(use-package company-jedi
  :init
  (defun fix-company-ac-conflict ()
    (when (fboundp 'auto-complete-mode)
      (auto-complete-mode -1)))
  (add-hook 'elpy-mode-hook 'fix-company-ac-conflict)

  (defun register-company-jedi-backend ()
    (add-to-list 'company-backends 'company-jedi))
  (add-hook 'after-init-hook 'register-company-jedi-backend)

  :bind (:map python-mode-map
              ("C-c ." . jedi:goto-definition)
              ("C-c ," . jedi:goto-definition-pop-marker)
              ("C-c ?" . jedi:show-doc)
              ("C-c <tab>" . company-jedi)))

(use-package importmagic
  :commands importmagic-mode
  :config
  (add-hook 'pyvenv-post-activate-hooks 'importmagic-mode))

(use-package sphinx-doc
  :commands sphinx-doc-mode
  :hook (python-mode . sphinx-doc-mode))


;;; JavaScript
;;; ----------

(use-package js2-mode
  :mode "\\.js\\'"
  :custom
  (js2-highlight-level 3)
  (js2-mode-assume-strict t)
  (js2-strict-trailing-comma-warning nil))

(use-package js2-refactor
  :commands js2-refactor-mode
  :hook (js2-mode . js2-refactor-mode)
  :config
  (js2r-add-keybindings-with-prefix "C-c m r"))

(use-package tern
  :commands tern-mode
  :hook (js2-mode . tern-mode))

(use-package company-tern
  :config
  (add-to-list 'company-backends 'company-tern))


;;; misc markup languages: HTML, Markdown, YAML, etc
;;; ------------------------------------------------

(use-package web-mode
  :mode "\\.html?\\'" "\\.css?\\'"
  :custom
  (sgml-basic-offset 4))

;; Highlight colors inside the HTML/CSS code.
(use-package rainbow-mode
  :commands rainbow-mode
  :hook (web-mode . rainbow-mode))

(use-package company-web
  :functions company-web-html
  :init
  (defun use-company-web-html-backend
      (setq-local company-backends '(company-web-html)))
  (add-hook 'web-mode-hook 'use-company-web-html-backend))

(use-package restclient
  :mode ("\\.http\\'" . restclient-mode))

(use-package markdown-mode
  :mode "\\.md\\'")

(use-package yaml-mode
  :mode "\\.yml\\'")

(use-package csv-mode
  :mode "\\.csv\\'")

(use-package json-mode
  :mode "\\.json\\'")

(use-package dockerfile-mode
  :mode "Dockerfile")


;;; ============
;;; Various Junk
;;; ============

(use-package elfeed
  :functions elfeed
  :config
  (define-key elfeed-search-mode-map (kbd "j") #'next-line)
  (define-key elfeed-search-mode-map (kbd "k") #'previous-line)

  (defface elfeed-unread
    '((t :foreground "grey85" :weight bold))
    "Marks unread entries in Elfeed."
    :group 'elfeed)

  (defface elfeed-important
    '((t :foreground "light coral"))
    "Marks important entries in Elfeed."
    :group 'elfeed)

  (defface elfeed-bright
    '((t :foreground "white"))
    "Highlights entries in Elfeed."
    :group 'elfeed)

  (defface elfeed-dim
    '((t :foreground "grey75"))
    "Marks non-important/dimmed entries in Elfeed."
    :group 'elfeed)

  (push '(youtube elfeed-dim) elfeed-search-face-alist)
  (push '(audio elfeed-dim) elfeed-search-face-alist)

  (setq
   elfeed-search-face-alist
   '((youtube elfeed-dim)
     (audio elfeed-dim)
     (comic elfeed-dim)
     (planet elfeed-dim)
     (procrastinate elfeed-dim)
     (clojure-job elfeed-bright)
     (important elfeed-important)
     (unread elfeed-unread)))

  (setq-default elfeed-search-filter "@1-week-ago +unread")


  (defvar youtube-feed-format
    '(("^UC" . "https://www.youtube.com/feeds/videos.xml?channel_id=%s")
      ("^PL" . "https://www.youtube.com/feeds/videos.xml?playlist_id=%s")
      ("" . "https://www.youtube.com/feeds/videos.xml?user=%s")))

  (defun elfeed--expand (listing)
    "Expand feed URLs depending on their tags."
    (cl-destructuring-bind (url . tags) listing
      (cond
        ((member 'youtube tags)
         (let* ((case-fold-search nil)
                (test (lambda (s r) (string-match-p r s)))
                (format (cl-assoc url youtube-feed-format :test test)))
           (cons (format (cdr format) url) tags)))
        (listing))))

   (defmacro elfeed-config-with-youtube (&rest feeds)
     "Minimizes feed listing indentation without being weird about it."
     (declare (indent 0))
     `(setf elfeed-feeds (mapcar #'elfeed--expand ',feeds)))

  (elfeed-config-with-youtube
    ("https://www.debian.org/News/news" debian news)
    ("http://blog.ioactive.com/feeds/posts/default" security)
    ("http://possiblywrong.wordpress.com/feed/" math)
    ("https://marckhoury.github.io/feed.xml" cs)
    ("https://feeds.feedburner.com/codinghorror?format=xml" dev)
    ("http://feeds.feedblitz.com/daedtech/www" dev)
    ("https://pragprog.com/feed/global/news.rss" dev books)
    ("http://thecodist.com/rss.php" dev)
    ("http://firefly.nu/feeds/all.atom.xml" dev)
    ("http://irreal.org/blog/?feed=rss2")
    ("http://mortoray.com/feed/" dev)
    ("https://danluu.com/atom.xml" dev)
    ("https://utcc.utoronto.ca/~cks/space/blog/?atom" dev)
    ("https://flapenguin.me/atom.xml" dev)
    ("https://www.hackerfactor.com/blog/rss.php?version=2.0" dev)
    ("https://nickdesaulniers.github.io/atom.xml" dev)
    ("http://blog.plover.com/index.atom" dev)
    ("https://martinfowler.com/feed.atom" dev)
    ("http://use-the-index-luke.com/blog/feed" dev db)
    ("http://syndication.thedailywtf.com/TheDailyWtf" dev)
    ("https://www.joelonsoftware.com/feed/" dev)
    ("https://mechanical-sympathy.blogspot.com/feeds/posts/default" dev)
    ("https://rcoh.me/index.xml" dev)
    ("https://ebanoe.it/feed/" procrastinate)
    ("https://projectfailures.wordpress.com/feed/" dev)
    ;; emacs
    ("http://planet.emacsen.org/atom.xml" emacs dev planet)
    ("http://nullprogram.com/feed/" emacs dev)
    ("https://twitrss.me/twitter_user_to_rss/?user=ecotd" emacs dev)
    ("http://pragmaticemacs.com/feed/" emacs dev)
    ;; clojure
    ("http://planet.clojure.in/atom.xml" clojure dev planet)
    ("https://www.reddit.com/r/clojure/.rss" clojure dev)
    ("http://feeds.cognitect.com/blog/feed.rss" clojure dev product)
    ;; hacker news
    ("https://hnrss.org/frontpage?count=10&points=300" procrastinate)
    ;; youtube
    ("UCqzQTbg2vsBc0a50_ZRZecQ" youtube) ; Феофанов
    ;; math & science
    ("1veritasium" youtube)
    ("UCYO_jab_esuFRV4b17AJtAw" youtube) ; 3Blue1Brown
    ("UCsXVk37bltHxD1rDPwtNM8Q" youtube) ; Kurzgesagt – In a Nutshell
    ("UC9yt3wz-6j19RwD5m5f6HSg" youtube) ; Think Twice
    ("LeiosOS" youtube)
    ("Taylorns34" youtube)
    ;; programming
    ("UCkRmQ_G_NbdbCQMpALg6UPg" youtube) ; emacs rocks
    ("UCiO3nb3sN5GsZUIyxrgXh0Q" youtube) ; Steven Yi
    ("UCaLlzGqiPE2QRj6sSOawJRg" youtube) ; Clojure TV
    ("UCOTrRnxBOllb9UHLuap_lPg" youtube) ; ClojureD
    ("UC_QIfHvN9auy2CoOdSfMWDw" youtube) ; Strange Loop
    ("mzamansky" youtube)                ; Mike Zamansky - Emacs stuff
    ("Misophistful" youtube)             ; Clojure screencast
    ;; music
    ("UCAeBMC784ACIc79rHl8419Q" youtube) ; fredguitarist
    ("UCRmm7T6mAm-QBNvnEaIl3AA" youtube) ; Nick Johnston
    ("UCTk9SwdLyWM6qaYQH1hq2fw" youtube) ; Plog
    ("UCm-6Ol9vPm86xglajhpW-hQ" youtube) ; Remco's Grove Lab
    ("UC_r7KC5gG-c4VBCBWodqEzw" youtube) ; The Helix Nebula
    ("UCnkp4xDOwqqJD7sSM3xdUiQ" youtube) ; Adam Neely
    ("UCy0am66jHf8y1hkWYSIpdww" youtube) ; Сергей Головин

    ;; home improvement
    ("UCV-Zw498m4hsDuBsK3siN7g" youtube) ; Лужецкий
    ("UCO4ZN0tVHNMmhMOmpcqNgsw" youtube) ; Сантехник НН
    ("UC9ekLOG8mJkkcXxp1A3GkCg" youtube) ; Пекарь
    ("UCqE_m97zH2NdVg91oQUJWuA" youtube) ; dretun
    ("UC4qbmWcdf-0OI9RQwgllM8g" youtube) ; Шайтер
    ("UChXkEa7OYBhffjVn4QV8K9g" youtube) ; Ткачёв
    ("UCSOLPJsVQuDcoBDFurmIrlg" youtube) ; Speed Quality
    ("UCSRNLMsKZTe2Tm3vQ9HrxNA" youtube) ; Сергей Кодолов

    ;; job
    ("https://www.upwork.com/ab/feed/topics/atom?securityToken=f1134547ce1b61953c71f9072561984647153c174aac5232e4cd022255eaca8ced4e195305544304b52f3719779a952af3e4c0da15879c015b9f82604e945b40&userUid=424259232341409792&orgUid=424259232345604097&topic=2376825" job clojure-job)
    )

  (add-hook 'elfeed-new-entry-hook
            (elfeed-make-tagger :before "7 days ago"
                                :remove 'unread))
  (add-hook 'elfeed-new-entry-hook
            (elfeed-make-tagger :feed-url "upwork\\.com"
                                :before "1 day ago"
                                :remove 'unread)))


;; Auto-generated
(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(package-selected-packages
   (quote
    (clj-refactor cider sphinx-doc auto-virtualenvwrapper pyvenv ztree zop-to-char zenburn-theme yasnippet-snippets yaml-mode yagist ws-butler which-key web-mode use-package undo-tree typescript-mode toggle-quotes smartparens smart-mode-line smart-comment restclient realgud rainbow-mode rainbow-delimiters pythonic python-mode python-docstring pyimport projectile-direnv poporg paradox neotree mic-paren markdown-mode magit kibit-helper json-mode js2-refactor jedi importmagic iedit hungry-delete highlight-thing highlight-parentheses highlight-numbers helm-swoop helm-projectile helm-ls-git helm-descbinds helm-cider helm-ag goto-last-change git-timemachine git-link flycheck-pycheckers flycheck-clojure expand-region exec-path-from-shell elpy elfeed dumb-jump drag-stuff dockerfile-mode diff-hl dashboard csv-mode copy-as-format company-web company-try-hard company-tern company-statistics company-quickhelp company-jedi clojure-snippets clojure-mode-extra-font-locking cljr-helm clean-aindent-mode avy ag)))
 '(paradox-automatically-star t t)
 '(paradox-execute-asynchronously t t)
 '(paradox-github-token t t)
 '(quote
   (custom-safe-themes
    (quote
     ("c74e83f8aa4c78a121b52146eadb792c9facc5b1f02c917e3dbb454fca931223" default))))
 '(safe-local-variable-values
   (quote
    ((python-shell-extra-pythonpaths "~/ws/v2-server")
     (python-shell-extra-pythonpaths list "~/ws/v2-server")
     (python-environment-virtualenv . "v2-server")
     (python-shell-virtualenv-root . "~/.local/venvs/v2-server")
     (python-shell-extra-pythonpaths . "~/ws/v2-server")
     (python-environment-default-root-name . "~/.local/venvs/v2-server"))))
 '(sml/no-confirm-load-theme t)
 '(sml/theme (quote respectful))
 '(use-package-always-ensure t)
 '(zenburn-override-colors-alist
   (quote
    (("zenburn-bg-2" . "grey9")
     ("zenburn-bg-1" . "grey12")
     ("zenburn-bg-05" . "grey14")
     ("zenburn-bg" . "grey18")
     ("zenburn-bg+05" . "grey21")
     ("zenburn-bg+1" . "grey23")
     ("zenburn-bg+2" . "grey26")
     ("zenburn-bg+3" . "grey29")))))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(default ((t (:inherit nil :stipple nil :background "grey18" :foreground "#DCDCCC" :inverse-video nil :box nil :strike-through nil :overline nil :underline nil :slant normal :weight normal :height 98 :width normal :foundry "unknown" :family "Liberation Mono"))))
 '(cider-deprecated-face ((t (:background "saddle brown"))))
 '(company-tooltip-selection ((t (:background "grey25" :foreground "white"))))
 '(diredp-compressed-file-name ((t (:foreground "deep sky blue"))))
 '(flymake-errline ((t (:underline nil))))
 '(flymake-warnline ((t (:underline nil))))
 '(hi-yellow ((t (:background "grey25" :foreground "grey85"))))
 '(highlight-indentation-face ((t (:foreground "grey40"))))
 '(mode-line ((t (:background "grey25" :foreground "grey90" :box (:line-width -1 :color "grey40")))))
 '(mode-line-highlight ((t (:box (:line-width -1 :color "grey50" :style released-button)))))
 '(mode-line-inactive ((t (:background "grey15" :foreground "grey50" :box (:line-width -1 :color "grey30"))))))
(put 'scroll-left 'disabled nil)
