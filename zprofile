# user-level /bin
PATH="$HOME/bin:$HOME/.local/bin:${PATH}"

# disable GVFS auto-mounting
export GVFS_DISABLE_FUSE=1
gconftool --type Boolean --set /apps/nautilus/preferences/media_automount  false

# fix Java fonts
export _JAVA_OPTIONS='-Dawt.useSystemAAFontSettings=lcd'
export _JAVA_AWT_WM_NONREPARENTING=1
export JAVA_HOME="$HOME/.local/jdk"
PATH="$JAVA_HOME/bin:${PATH}"

# fix eclipse
export SWT_GTK3=0

export BROWSER=chromium
export EDITOR="emacsclient --tty --alternate-editor=vim"
export VISUAL="emacsclient --create-frame --alternate-editor=emacs"

# compile with optimization by default
export CFLAGS="-march=native -O2 -pipe"
export CXXFLAGS="${CFLAGS}"
export LDFLAGS="-Wl,-O1"

# Ruby
export GEM_PATH="$HOME/.local/gems"
export GEM_HOME="$HOME/.local/gems"
PATH="$HOME/.local/gems/bin:${PATH}"

# Node.js
PATH="$HOME/.local/node/bin:${PATH}"

# Python
export PROJECT_HOME="${HOME}/ws/"
export WORKON_HOME="${HOME}/.local/venvs/"
export VIRTUALENVWRAPPER_PYTHON=$(which python3)
export VIRTUALENVWRAPPER_SCRIPT=$(which virtualenvwrapper.sh)

# export new PATH after setting up various Node/Python/Ruby envs
# after this line, no modification of PATH should be done
export PATH

# setup GPG/gpg-agent
export GPGKEY=D58222C84FB1309F
gpg-agent --daemon --enable-ssh-support --write-env-file "${HOME}/.gpg-agent-info"
if [ -f "${HOME}/.gpg-agent-info" ]; then
  . "${HOME}/.gpg-agent-info"
  export GPG_AGENT_INFO
  export SSH_AUTH_SOCK
  export SSH_AGENT_PID
fi
GPG_TTY=$(tty)
export GPG_TTY

# pass: extensions in ~/.password-store/extensions
PASSWORD_STORE_ENABLE_EXTENSIONS=true

# finally, start X at login on tty1
[[ -z $DISPLAY ]] && [[ $(tty) = /dev/tty1 ]] && startx

